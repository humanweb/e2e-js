Feature: Testing tools
  In order to use the e2e-js image
  As a user
  I need to check if it works

  Scenario: Checking the homepage
    Given my e2e browser is ready
    When it visits the homepage
    Then it should see "Example Domain"
