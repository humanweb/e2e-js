const { setWorldConstructor } = require('cucumber');
const puppeteer = require('puppeteer-core');
const { expect } = require('chai');
const { url } = require('./config');

class CustomWorld {
  constructor(parameters) {
  }

  async launchBrowser() {
    this.browser = await puppeteer.launch({
      headless: true,
      executablePath: '/usr/bin/chromium',
      args: ['--no-sandbox'],
    });
  }

  async closeBrowser() {
    await this.browser.close();
  }

  async browserIsReady() {
    let browserVersion = await this.browser.version();
    expect(browserVersion).to.include('HeadlessChrome');
  }

  async getHomePage() {
    let page = await this.browser.newPage();
    await page.goto(url);
    this.pageContent = await page.content();
  }

  assessPageContentContains(substring) {
    expect(this.pageContent).to.include(substring);
  }
}

setWorldConstructor(CustomWorld);
