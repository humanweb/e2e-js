const { Before, Given, When, Then, After } = require('cucumber');

Before(async function (testCase) {
  return await this.launchBrowser();
});

After(async function () {
  return await this.closeBrowser();
});

Given('my e2e browser is ready', async function () {
  return this.browserIsReady();
});

When('it visits the homepage', function () {
  return this.getHomePage();
});

Then('it should see {string}', async function (string) {
  return this.assessPageContentContains(string);
});
