# e2e-js

The source code for [the humanweb/e2e-js docker image](https://hub.docker.com/r/humanweb/e2e-js).

The image contains end to end testing tools in javascript.

 - nodejs
 - chromium
 - puppeteer-core
 - cucumberjs
 - chaijs

## usage

 - `make run-e2e-tests` to run tests directly
 - `make log-in-e2e-js-container` to log into the container (and run tests manually)

See `Makefile` and `scripts` directory for details.

