run-e2e-tests:
	docker run --rm -ti \
	-v ${PWD}/.env:/e2e-js/.env \
	-v ${PWD}/features:/e2e-js/features \
	-v ${PWD}/package.json:/e2e-js/package.json \
	humanweb/e2e-js:1.8.0 npm run test:pretty

log-in-e2e-js-container:
	docker run --rm -ti \
	-v ${PWD}/.env:/e2e-js/.env \
	-v ${PWD}/features:/e2e-js/features \
	-v ${PWD}/package.json:/e2e-js/package.json \
	humanweb/e2e-js:1.8.0 bash

build:
	docker build -t humanweb/e2e-js:1.8.0 .
