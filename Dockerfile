FROM node:12.18.0-buster

RUN apt-get update && apt-get install -y chromium

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install

ENV PATH="/node_modules/.bin/:$PATH"

WORKDIR e2e-js
